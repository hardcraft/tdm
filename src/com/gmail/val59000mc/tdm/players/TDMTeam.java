package com.gmail.val59000mc.tdm.players;

import org.bukkit.ChatColor;
import org.bukkit.Location;

import com.gmail.val59000mc.hcgameslib.players.HCTeam;
import com.gmail.val59000mc.spigotutils.Locations.LocationBounds;

public class TDMTeam extends HCTeam{

	private LocationBounds noFallZone;
	
	public TDMTeam(String name, ChatColor color, Location spawnpoint) {
		super(name, color, spawnpoint);
	}

	public LocationBounds getNoFallZone() {
		return noFallZone;
	}

	public void setNoFallZone(LocationBounds noFallZone) {
		this.noFallZone = noFallZone;
	}
	
	

}

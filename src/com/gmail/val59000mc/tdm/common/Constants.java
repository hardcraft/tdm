package com.gmail.val59000mc.tdm.common;

public class Constants {

	public static final int DEFAULT_TEAMS_NUMBER = 2;
	public static final String RED_TEAM = "Rouge";
	public static final String BLUE_TEAM = "Bleu";
	public static final String YELLOW_TEAM = "Jaune";
	public static final String GREEN_TEAM = "Vert";
	
	public static final boolean DEFAULT_ENABLE_NIGHT_VISION = true;
	
	public static final int DEFAULT_KILL_LIMIT = 30;
	
	public static final long DEFAULT_TIME_TICKS = 6000;
	
}

package com.gmail.val59000mc.tdm.items;

import java.util.List;

import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;

import com.gmail.val59000mc.spigotutils.items.ItemBuilder;
import com.gmail.val59000mc.spigotutils.items.LeatherArmorMetaBuilder;
import com.google.common.collect.Lists;

public class TDMItems {
	
	public static List<ItemStack> getArmorItems(Color color) {
		
		return Lists.newArrayList(
			new ItemBuilder(Material.LEATHER_HELMET).buildMeta(LeatherArmorMetaBuilder.class).withColor(color).withUnbreakable(true).item().build(),
			new ItemBuilder(Material.IRON_CHESTPLATE).buildMeta().withUnbreakable(true).item().build(),
			new ItemBuilder(Material.IRON_LEGGINGS).buildMeta().withUnbreakable(true).item().build(),
			new ItemBuilder(Material.IRON_BOOTS).buildMeta().withUnbreakable(true).item().build()
		);
		
	}
	
	public static List<ItemStack> getInventoryItems() {
		
		return Lists.newArrayList(
			new ItemBuilder(Material.STONE_SWORD).buildMeta().withEnchant(Enchantment.DAMAGE_ALL, 1, true).withUnbreakable(true).item().build(),
			new ItemBuilder(Material.BOW).buildMeta().withEnchant(Enchantment.ARROW_INFINITE, 1, true).withUnbreakable(true).item().build(),
			new ItemStack(Material.COOKED_BEEF, 5),
			new ItemStack(Material.GOLDEN_APPLE, 2),
			new ItemStack(Material.ARROW, 1)
		);
		
	}
	
}

package com.gmail.val59000mc.tdm.listeners;

import java.sql.SQLException;

import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;

import com.gmail.val59000mc.hcgameslib.api.HCMySQLAPI;
import com.gmail.val59000mc.hcgameslib.common.Log;
import com.gmail.val59000mc.hcgameslib.database.HCPlayerDBInsertedEvent;
import com.gmail.val59000mc.hcgameslib.database.HCPlayerDBPersistedSessionEvent;
import com.gmail.val59000mc.hcgameslib.events.HCAfterLoadEvent;
import com.gmail.val59000mc.hcgameslib.listeners.HCListener;
import com.gmail.val59000mc.spigotutils.Logger;
import com.gmail.val59000mc.tdm.players.TDMPlayer;

public class TDMMySQLListener extends HCListener{

	// Queries
	private String createTDMPlayerSQL;
	private String createTDMPlayerStatsSQL;
	private String insertTDMPlayerSQL;
	private String insertTDMPlayerStatsSQL;
	private String updateTDMPlayerGlobalStatsSQL;
	private String updateTDMPlayerStatsSQL;
	
	/**
	 * Load sql queries from resources files
	 */
	public void readQueries() {
		HCMySQLAPI sql = getMySQLAPI();
		if(sql.isEnabled()){
			createTDMPlayerSQL = sql.readQueryFromResource(getApi().getPlugin(),"resources/sql/create_tdm_player.sql");
			createTDMPlayerStatsSQL = sql.readQueryFromResource(getApi().getPlugin(),"resources/sql/create_tdm_player_stats.sql");
			insertTDMPlayerSQL = sql.readQueryFromResource(getApi().getPlugin(),"resources/sql/insert_tdm_player.sql");
			insertTDMPlayerStatsSQL = sql.readQueryFromResource(getApi().getPlugin(),"resources/sql/insert_tdm_player_stats.sql");
			updateTDMPlayerGlobalStatsSQL = sql.readQueryFromResource(getApi().getPlugin(), "resources/sql/update_tdm_player_global_stats.sql");
			updateTDMPlayerStatsSQL = sql.readQueryFromResource(getApi().getPlugin(), "resources/sql/update_tdm_player_stats.sql");
		}
	}
	
	/**
	 * Insert game if not exists
	 * @param e
	 */
	@EventHandler
	public void onGameFinishedLoading(HCAfterLoadEvent e){
		HCMySQLAPI sql = getMySQLAPI();
		
		readQueries();
		
		if(sql.isEnabled()){
			
			Bukkit.getScheduler().runTaskAsynchronously(getPlugin(), new Runnable() {
				@Override
				public void run() {
					
					try{
						
						sql.execute(sql.prepareStatement(createTDMPlayerSQL));
						
						sql.execute(sql.prepareStatement(createTDMPlayerStatsSQL));
												
					}catch(SQLException e){
						Logger.severe("Couldnt create tables for CTF stats or perks");
						e.printStackTrace();
					}
					
				}
			});
		}
	}
	
	/**
	 * Add new player if not exists when joining
	 * Update name (because it may change)
	 * Update current played game 
	 * @param e
	 */
	@EventHandler(priority=EventPriority.HIGHEST)
	public void onPlayerJoin(HCPlayerDBInsertedEvent e){
		HCMySQLAPI sql = getMySQLAPI();
		
		if(sql.isEnabled()){
			
			Bukkit.getScheduler().runTaskAsynchronously(getPlugin(), new Runnable() {
				@Override
				public void run() {

					try{ 
						String id = String.valueOf(e.getHcPlayer().getId());
						
						// Insert ctf player if not exists
						sql.execute(sql.prepareStatement(insertTDMPlayerSQL, id));
						
						// Insert ctf player stats if not exists
						sql.execute(sql.prepareStatement(insertTDMPlayerStatsSQL, id, sql.getMonth(), sql.getYear()));
						
					} catch (SQLException ex) {
						Log.severe("Couldn't find perks for player "+e.getHcPlayer().getName());
						ex.printStackTrace();
					}
				}
			});
		}
	}
	
	
	/**
	 * Save all players global data when game ends
	 * @param e
	 */
	@EventHandler
	public void onGameEnds(HCPlayerDBPersistedSessionEvent e){
		HCMySQLAPI sql = getMySQLAPI();
		
		if(sql.isEnabled()){
			
			TDMPlayer tdmPlayer = (TDMPlayer) e.getHcPlayer();
			
			// Launch one async task to save all data
			Bukkit.getScheduler().runTaskAsynchronously(getPlugin(), new Runnable() {
				@Override
				public void run() {

					try{
						// Update ctf player global stats
						sql.execute(sql.prepareStatement(updateTDMPlayerGlobalStatsSQL,
								String.valueOf(tdmPlayer.getTimePlayed()),
								String.valueOf(tdmPlayer.getKills()),
								String.valueOf(tdmPlayer.getDeaths()),
								String.valueOf(tdmPlayer.getMoney()),
								String.valueOf(tdmPlayer.getWins()),
								String.valueOf(tdmPlayer.getId())
						));
						
	
						// Update ctf player stats
						sql.execute(sql.prepareStatement(updateTDMPlayerStatsSQL,
								String.valueOf(tdmPlayer.getTimePlayed()),
								String.valueOf(tdmPlayer.getKills()),
								String.valueOf(tdmPlayer.getDeaths()),
								String.valueOf(tdmPlayer.getMoney()),
								String.valueOf(tdmPlayer.getWins()),
								String.valueOf(tdmPlayer.getId()),
								sql.getMonth(),
								sql.getYear()
						));
					
					}catch(SQLException e){
						Logger.severe("Couldnt update TDM player stats for player="+tdmPlayer.getName()+" uuid="+tdmPlayer.getUuid());
						e.printStackTrace();
					}
					
				}
			});
		}

	}
}

package com.gmail.val59000mc.tdm.listeners;

import org.bukkit.entity.EntityType;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockExplodeEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;

import com.gmail.val59000mc.hcgameslib.listeners.HCListener;

public class TDMBlockListener extends HCListener{
	
	@EventHandler
	public void onBlockBreak(BlockBreakEvent e){
		e.setCancelled(true);
	}

	@EventHandler
	public void onBlockPlace(BlockPlaceEvent e){
		e.setCancelled(true);
	}

	@EventHandler
	public void onBlockExplose(BlockExplodeEvent e){
		e.setCancelled(true);
	}
	
	@EventHandler
	public void onInteractItemFrame(PlayerInteractEntityEvent e){
		if(e.getRightClicked().getType().equals(EntityType.ITEM_FRAME)){
			e.setCancelled(true);
		}
	}

	@EventHandler
	public void onDamageItemFrame(EntityDamageEvent e){
		if(e.getEntity().getType().equals(EntityType.ITEM_FRAME)){
			e.setCancelled(true);
		}
	}

	@EventHandler
	public void onEntityExplode(EntityExplodeEvent e){
		e.setCancelled(true);
	}
	
}

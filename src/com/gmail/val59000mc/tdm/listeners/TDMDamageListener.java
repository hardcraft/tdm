package com.gmail.val59000mc.tdm.listeners;

import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.potion.PotionEffectType;

import com.gmail.val59000mc.hcgameslib.events.HCAfterLoadEvent;
import com.gmail.val59000mc.hcgameslib.listeners.HCListener;
import com.gmail.val59000mc.tdm.players.TDMPlayer;
import com.gmail.val59000mc.tdm.players.TDMTeam;

public class TDMDamageListener extends HCListener{
	
	@EventHandler
	public void afterLoad(HCAfterLoadEvent e){
		
	}
	
	
	/**
	 * Simple way to make player invincible when having resistance effect
	 */
	@EventHandler(priority = EventPriority.HIGHEST)
	public void onPlayerDamage(EntityDamageEvent e){
		if(e.getEntityType().equals(EntityType.PLAYER)){
			Player player = (Player) e.getEntity();
			if(player.hasPotionEffect(PotionEffectType.DAMAGE_RESISTANCE)){
				e.setCancelled(true);
			}else if(e.getCause().equals(DamageCause.FALL)){
				TDMPlayer tdmPlayer = (TDMPlayer) getPmApi().getHCPlayer(player);
				if(tdmPlayer != null && tdmPlayer.hasTeam() && ((TDMTeam) tdmPlayer.getTeam()).getNoFallZone().contains(player.getLocation())){
					e.setCancelled(true);
				}
			}
		}
	}
	
}

package com.gmail.val59000mc.tdm;

import java.io.File;

import org.bukkit.plugin.java.JavaPlugin;

import com.gmail.val59000mc.hcgameslib.api.HCGameAPI;
import com.gmail.val59000mc.hcgameslib.game.HCGame;
import com.gmail.val59000mc.spigotutils.Configurations;
import com.gmail.val59000mc.tdm.callbacks.TDMCallbacks;
import com.gmail.val59000mc.tdm.listeners.TDMBlockListener;
import com.gmail.val59000mc.tdm.listeners.TDMDamageListener;
import com.gmail.val59000mc.tdm.listeners.TDMMySQLListener;
import com.google.common.collect.Sets;

public class TDM extends JavaPlugin{
	
	public void onEnable(){

		this.getDataFolder().mkdirs();
		File config = Configurations.saveConfigDefaultsToFile(Configurations.getConfigFromResource(this, "config.yml"), new File(this.getDataFolder(),"config.yml"));
		File lang = Configurations.saveConfigDefaultsToFile(Configurations.getConfigFromResource(this, "lang.yml"), new File(this.getDataFolder(),"lang.yml"));
		
		HCGameAPI game = new HCGame.Builder("Team Deathmatch", this, config, lang)
				.withPluginCallbacks(new TDMCallbacks())
				.withDefaultListenersAnd(Sets.newHashSet(
						new TDMBlockListener(),
						new TDMDamageListener(),
						new TDMMySQLListener()
				))
				.build();
		game.loadGame();
	}
	
	public void onDisable(){
		
	}
}

package com.gmail.val59000mc.tdm.callbacks;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffectType;

import com.gmail.val59000mc.hcgameslib.api.impl.DefaultPluginCallbacks;
import com.gmail.val59000mc.hcgameslib.players.HCPlayer;
import com.gmail.val59000mc.hcgameslib.players.HCTeam;
import com.gmail.val59000mc.hcgameslib.stuff.Stuff;
import com.gmail.val59000mc.spigotutils.Effects;
import com.gmail.val59000mc.spigotutils.Locations.LocationBounds;
import com.gmail.val59000mc.spigotutils.Parser;
import com.gmail.val59000mc.tdm.common.Constants;
import com.gmail.val59000mc.tdm.items.TDMItems;
import com.gmail.val59000mc.tdm.players.TDMPlayer;
import com.gmail.val59000mc.tdm.players.TDMTeam;
import com.google.common.collect.Lists;

public class TDMCallbacks extends DefaultPluginCallbacks{
	
	private boolean enableNightVision;
	private int killsLimit;
	
	@Override
	public void beforeLoad(){
		enableNightVision = getConfig().getBoolean("night-vision", Constants.DEFAULT_ENABLE_NIGHT_VISION);
		killsLimit = getConfig().getInt("kills-limit", Constants.DEFAULT_KILL_LIMIT);
	}
	
	@Override
	public HCTeam newHCTeam(String name, ChatColor color, Location spawnpoint){
		return new TDMTeam(name, color, spawnpoint);
	}
	
	@Override
	public HCPlayer newHCPlayer(Player player){
		return new TDMPlayer(player);
	}
	
	@Override
	public HCPlayer newHCPlayer(HCPlayer hcPlayer){
		return new TDMPlayer(hcPlayer);
	}
	
	@Override
	public List<HCTeam> createTeams() {
		List<HCTeam> teams = new ArrayList<HCTeam>();
		
		World world = getApi().getWorldConfig().getWorld();
		
		FileConfiguration cfg = getConfig();
		
		TDMTeam redTeam = (TDMTeam) newHCTeam(Constants.RED_TEAM, ChatColor.RED, Parser.parseLocation(world, cfg.getString("teams.red")));
		redTeam.setNoFallZone(new LocationBounds(
				Parser.parseLocation(world, cfg.getString("no-fall-zones.red.min")), 
				Parser.parseLocation(world, cfg.getString("no-fall-zones.red.max"))
			)
		);
		
		TDMTeam blueTeam = (TDMTeam) newHCTeam(Constants.BLUE_TEAM, ChatColor.BLUE, Parser.parseLocation(world, getConfig().getString("teams.blue")));
		blueTeam.setNoFallZone(new LocationBounds(
				Parser.parseLocation(world, cfg.getString("no-fall-zones.blue.min")), 
				Parser.parseLocation(world, cfg.getString("no-fall-zones.blue.max"))
			)
		);
		
		TDMTeam yellowTeam = (TDMTeam) newHCTeam(Constants.YELLOW_TEAM, ChatColor.YELLOW, Parser.parseLocation(world, cfg.getString("teams.yellow")));
		yellowTeam.setNoFallZone(new LocationBounds(
				Parser.parseLocation(world, cfg.getString("no-fall-zones.yellow.min")), 
				Parser.parseLocation(world, cfg.getString("no-fall-zones.yellow.max"))
			)
		);
		
		TDMTeam greenTeam = (TDMTeam) newHCTeam(Constants.GREEN_TEAM, ChatColor.GREEN, Parser.parseLocation(world, cfg.getString("teams.green")));
		greenTeam.setNoFallZone(new LocationBounds(
				Parser.parseLocation(world, cfg.getString("no-fall-zones.green.min")), 
				Parser.parseLocation(world, cfg.getString("no-fall-zones.green.max"))
			)
		);
		
		if(getConfig().getInt("teams.number",Constants.DEFAULT_TEAMS_NUMBER) == 4){
			// 4 teams
			teams.add(redTeam);
			teams.add(blueTeam);
			teams.add(yellowTeam);
			teams.add(greenTeam);
			
		}else{
			// 2 teams
			teams.add(redTeam);
			teams.add(blueTeam);
		}

		return teams;
	}
	
	@Override
	public List<Stuff> createStuffs() {
		
		return Lists.newArrayList(
			new Stuff.Builder(Constants.RED_TEAM)
				.addArmorItems(TDMItems.getArmorItems(Color.RED))
				.addInventoryItems(TDMItems.getInventoryItems())
				.build(),
			new Stuff.Builder(Constants.BLUE_TEAM)
				.addArmorItems(TDMItems.getArmorItems(Color.BLUE))
				.addInventoryItems(TDMItems.getInventoryItems())
				.build(),
			new Stuff.Builder(Constants.YELLOW_TEAM)
				.addArmorItems(TDMItems.getArmorItems(Color.YELLOW))
				.addInventoryItems(TDMItems.getInventoryItems())
				.build(),
			new Stuff.Builder(Constants.GREEN_TEAM)
				.addArmorItems(TDMItems.getArmorItems(Color.GREEN))
				.addInventoryItems(TDMItems.getInventoryItems())
				.build()
	    );
	}
	
	@Override
	public void assignStuffToPlayer(HCPlayer hcPlayer){
		if(hcPlayer.getStuff() == null && hcPlayer.hasTeam() && hcPlayer.isOnline()){
			hcPlayer.setStuff(getApi().getItemsAPI().getStuff(hcPlayer.getTeam().getName()));
		}
	}
	
	@Override
	public void handleDeathEvent(PlayerDeathEvent event, HCPlayer hcKilled) {

		super.handleDeathEvent(event, hcKilled);
		
		removePlayerDeathDrops(event.getDrops());
		
	}

	@Override
	public void handleKillEvent(PlayerDeathEvent event, HCPlayer hcKilled, HCPlayer hcKiller) {

		super.handleKillEvent(event, hcKilled, hcKiller);
		
		removePlayerDeathDrops(event.getDrops());
		
		checkIfTeamHasWon(hcKiller.getTeam());
		
	}
	
	private void checkIfTeamHasWon(HCTeam team) {
		if(team.getKills() >= killsLimit){
			getApi().endGame(team);
		}
	}
	
	private void removePlayerDeathDrops(List<ItemStack> drops){
		Iterator<ItemStack> it = drops.iterator();
		while(it.hasNext()){
			ItemStack drop = it.next();
			switch(drop.getType()){
				case GOLDEN_APPLE:
				case COOKED_BEEF:
					// don't remove
					break;
				default:
					it.remove();
					break;
			}
		}
	}

	@Override
	public void respawnPlayer(HCPlayer hcPlayer) {

		if(hcPlayer.isOnline()){
			hcPlayer.getPlayer().setSaturation(20);
			addPlayerSpawningEffects(hcPlayer.getPlayer());
			getApi().getItemsAPI().giveStuffItemsToPlayer(hcPlayer);
		}
		
	}
	
	@Override
	public void startPlayer(HCPlayer hcPlayer) {
		super.startPlayer(hcPlayer);
		if(hcPlayer.isOnline()){
			hcPlayer.getPlayer().setGameMode(GameMode.SURVIVAL);
			addPlayerSpawningEffects(hcPlayer.getPlayer());
		}
	}
	
	private void addPlayerSpawningEffects(Player player){
		Effects.add(player, PotionEffectType.DAMAGE_RESISTANCE, 50,  200);
		Effects.addPermanent(player, PotionEffectType.SPEED, 0);
		if(enableNightVision){
			Effects.addPermanent(player, PotionEffectType.NIGHT_VISION, 0);
		}
	}
	
}
